package com.slickcode.midnightdanger.utils;

import android.opengl.Matrix;

public class Vector {
    private final float x, y, z;

    public Vector(float xyz) {
        this.x = this.y = this.z = xyz;
    }
    public Vector(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Vector(float[] vector) {
        assert vector.length >= 3 && vector.length <= 4;
        this.x = vector[0];
        this.y = vector[1];
        this.z = vector[2];
    }

    public float getX() {
        return x;
    }
    public float getY() {
        return y;
    }
    public float getZ() {
        return z;
    }
    public float getLength() {
        return (float) Math.sqrt(x*x + y*y + z*z);
    }

    public static Vector subtract(Vector a, Vector b) {
        return new Vector(a.x - b.x, a.y - b.y, a.z - b.z);
    }
    public static Vector normalize(Vector v) {
        float length = v.getLength();
        return new Vector(v.x / length, v.y / length, v.z / length);
    }
    public static Vector cross(Vector a, Vector b) {
        return new Vector(
        a.y * b.z - b.y * a.z,
        a.z * b.x - b.z * a.x,
        a.x * b.y - b.x * a.y);
    }

    public float[] toFloatArray() {
        return new float[] {x, y, z};
    }
    public float[] toFloat4Array() {
        return new float[] {x, y, z, 1.0f};
    }
    public Vector toWorldCoords(ModelParameters modelParameters) {
        float[] modelMatrix = modelParameters.createModelMatrix();
        float[] pos = new float[4];
        Matrix.multiplyMV(pos, 0, modelMatrix, 0, this.toFloat4Array(), 0);
        return new Vector(pos);
    }
}
