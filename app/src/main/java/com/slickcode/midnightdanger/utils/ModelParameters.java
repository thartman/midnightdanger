package com.slickcode.midnightdanger.utils;

import android.opengl.Matrix;

public class ModelParameters {
    private Vector position;
    private Rotation rotation;
    private Vector scale;

    public static class Rotation extends Vector {
        private float angle;

        public Rotation() {
            super(1.0f , 1.0f, 1.0f);
            this.angle = 0.0f;
        }

        public Rotation(float x, float y, float z, float angle) {
            super(x, y, z);
            this.angle = angle;
        }

        public void setAngle(float angle) {
            this.angle = angle;
        }
        public void setDirection(Vector direction) {

        }

        public float getAngle() {
            return angle;
        }
    }

    public ModelParameters(Vector position, Rotation rotation, Vector scale) {
        this.position = position;
        if(position == null) {
            this.position = new Vector(0.0f, 0.0f, 0.0f);
        }
        this.rotation = rotation;
        if(rotation == null) {
            this.rotation = new Rotation();
        }
        this.scale = scale;
        if(scale == null) {
            this.scale = new Vector(1.0f, 1.0f, 1.0f);
        }
    }

    public void setPosition(Vector position) {
        this.position = position;
    }
    public void setRotation(Rotation rotation) {
        this.rotation = rotation;
    }

    public Vector getPosition() {
        return position;
    }
    public Rotation getRotation() {
        return rotation;
    }
    public Vector getScale() {
        return scale;
    }

    public float[] createModelMatrix() {
        float[] modelMatrix = new float[16];
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.translateM(modelMatrix, 0,
                getPosition().getX(),
                getPosition().getY(),
                getPosition().getZ());
        Matrix.scaleM(modelMatrix, 0,
                getScale().getX(),
                getScale().getY(),
                getScale().getZ());
        Matrix.rotateM(modelMatrix, 0, getRotation().getAngle(),
                getRotation().getX(),
                getRotation().getY(),
                getRotation().getZ());
        return modelMatrix;
    }
    public float[] createRotationMatrix() {
        float[] rotationMatrix = new float[16];
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.rotateM(rotationMatrix, 0, getRotation().getAngle(),
                getRotation().getX(),
                getRotation().getY(),
                getRotation().getZ());
        return rotationMatrix;
    }
}
