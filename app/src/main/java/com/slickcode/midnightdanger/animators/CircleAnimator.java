package com.slickcode.midnightdanger.animators;

import com.slickcode.midnightdanger.utils.ModelParameters;
import com.slickcode.midnightdanger.utils.Vector;

public class CircleAnimator extends Animator {
    private Vector center;
    private float radius;

    public CircleAnimator(Animatable object, long timespan, Vector center, float radius) {
        super(object, timespan);
        this.center = center;
        this.radius = radius;
    }

    @Override
    public long onDrawFrame() {
        long time = super.onDrawFrame();
        float angle = (float) time / (float) getTimespan() * 2.0f * (float) Math.PI;

        getObject().setPosition(new Vector(center.getX() + (float) Math.sin(angle) * radius,
                center.getY(),
                center.getZ() + (float) Math.cos(angle) * radius));
        getObject().setRotation(new ModelParameters.Rotation(0.0f, 1.0f, 0.0f, 360.0f / getTimespan() * time - 90.0f));
        return time;
    }
}
