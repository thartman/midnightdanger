package com.slickcode.midnightdanger.animators;

public abstract class Animator {
    private long timespan;
    private Animatable object;

    public Animator(Animatable object, long timespan) {
        assert timespan > 0;
        this.object = object;
        this.timespan = timespan;
    }

    public long onDrawFrame() {
        return System.currentTimeMillis() % timespan;
    }

    public long getTimespan() {
        return timespan;
    }

    public Animatable getObject() {
        return object;
    }
}
