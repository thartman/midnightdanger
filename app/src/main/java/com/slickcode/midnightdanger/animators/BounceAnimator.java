package com.slickcode.midnightdanger.animators;

import com.slickcode.midnightdanger.utils.ModelParameters;

public class BounceAnimator extends Animator {
    private float maxAngle;

    public BounceAnimator(Animatable object, long timespan, float maxAngle) {
        super(object, timespan);
        this.maxAngle = maxAngle;
    }

    @Override
    public long onDrawFrame() {
        long time = super.onDrawFrame();
        float timepoint = (float) time / (float) getTimespan();
        float angle = (float) Math.sin(timepoint * 2.0f * Math.PI) * maxAngle;
        getObject().setRotation(new ModelParameters.Rotation(0.0f, 1.0f, 0.0f, angle));
        return time;
    }
}
