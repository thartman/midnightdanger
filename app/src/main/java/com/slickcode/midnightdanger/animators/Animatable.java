package com.slickcode.midnightdanger.animators;

import com.slickcode.midnightdanger.utils.ModelParameters;
import com.slickcode.midnightdanger.utils.Vector;

public interface Animatable {
    void setPosition(Vector position);
    void setRotation(ModelParameters.Rotation rotation);
}
