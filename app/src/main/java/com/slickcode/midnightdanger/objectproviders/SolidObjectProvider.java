package com.slickcode.midnightdanger.objectproviders;

public class SolidObjectProvider extends AbstractObjectProvider {
    private float[] color;

    public SolidObjectProvider(float[] vertices, float[] normals, float[] color) {
        assert color.length == 3;
        this.vertices = vertices;
        this.normals = normals;
        this.color = color;
    }

    public float[] getColor() {
        return color;
    }
}
