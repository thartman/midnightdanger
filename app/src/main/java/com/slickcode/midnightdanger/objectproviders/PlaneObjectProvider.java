package com.slickcode.midnightdanger.objectproviders;

public class PlaneObjectProvider extends SolidObjectProvider {
    public PlaneObjectProvider(float xSize, float zSize, float[] color) {
        super(new float[] {-xSize/2, 0, -zSize/2,
                            -xSize/2, 0, zSize/2,
                            xSize/2, 0, zSize/2,
                            xSize/2, 0, -zSize/2,
                            -xSize/2, 0, -zSize/2,
                            xSize/2, 0, zSize/2},
                new float[] {0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f},
                color);
    }
}
