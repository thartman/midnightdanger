package com.slickcode.midnightdanger.objectproviders;

import android.util.Log;

import com.google.common.collect.Lists;

import org.apache.commons.lang3.ArrayUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ObjObjectProvider extends AbstractObjectProvider {
    // TODO: remove magic numbers
    public static final int COORDS_PER_VERTEX = 3;
    public static final int COORDS_PER_TEXTURE_VERTEX = 2;
    public static final int COORDS_PER_NORMAL_VERTEX = 3;
    private static final String TAG_OBJ = "ObjObjectProvider";
    // In OpenGL (0,0) is in top-left corner of bitmap
    private float[] texture;

    public ObjObjectProvider(InputStream is) throws IOException {
        List<Float> vertices = new ArrayList<>();
        List<Short> drawOrder = new ArrayList<>();
        is.mark(0);
        float[] textureCoords = readTextureCoords(is);
        is.reset();
        float[] normalCoords = readNormalCoords(is);
        is.reset();
        List<Float> texture = new ArrayList<>();
        List<Float> normal = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        while((line = reader.readLine()) != null) {
            String[] elements = line.split(" ");
            switch(elements[0]) {
                case "v":
                    if(elements.length != COORDS_PER_VERTEX + 1) {
                        Log.e(TAG_OBJ, "Invalid line format " + line);
                    }
                    vertices.addAll(Lists.transform(Arrays.asList(elements).subList(1, 4), Float::parseFloat));
                    break;
                case "f":
                    if(elements.length != COORDS_PER_VERTEX + 1) {
                        Log.e(TAG_OBJ, "Invalid line format " + line);
                    }
                    for(String s : Arrays.asList(elements).subList(1, 4)) {
                        String[] parts = s.split("/");
                        assert parts.length == 3;
                        drawOrder.add((short) (Short.parseShort(parts[0]) - 1));

                        if(parts[1].length() > 0) {
                            int textureCoordNumber = Integer.parseInt(parts[1]) - 1;
                            texture.add(textureCoords[2 * textureCoordNumber]);
                            texture.add(textureCoords[2 * textureCoordNumber + 1]);
                        }

                        int normalCoordNumber = Integer.parseInt(parts[2]) - 1;
                        normal.add(normalCoords[3 * normalCoordNumber]);
                        normal.add(normalCoords[3 * normalCoordNumber + 1]);
                        normal.add(normalCoords[3 * normalCoordNumber + 2]);
                    }
            }
        }
        short[] drawOrderPrimitive = ArrayUtils.toPrimitive(drawOrder.toArray(new Short[0]));
        this.vertices = transformVerticesWithDrawOrder(ArrayUtils.toPrimitive(vertices.toArray(new Float[0])), drawOrderPrimitive);
        this.texture = texture.size() == 0 ? null : ArrayUtils.toPrimitive(texture.toArray(new Float[0]));
        this.normals = normal.size() == 0 ? null : ArrayUtils.toPrimitive(normal.toArray(new Float[0]));
    }

    private float[] readTextureCoords(InputStream is) throws IOException {
        List<Float> textureCoords = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        while((line = reader.readLine()) != null) {
            String[] elements = line.split(" ");
            if(elements[0].equals("vt")) {
                assert elements.length == 3;
                textureCoords.add(Float.parseFloat(elements[1]));
                // In OBJ (0,0) is in bottom-left corner of bitmap
                textureCoords.add(1 - Float.parseFloat(elements[2]));
            }
        }
        return textureCoords.size() == 0 ? null : ArrayUtils.toPrimitive(textureCoords.toArray(new Float[0]));
    }
    private float[] readNormalCoords(InputStream is) throws IOException {
        List<Float> normalCoords = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        while((line = reader.readLine()) != null) {
            String[] elements = line.split(" ");
            if(elements[0].equals("vn")) {
                assert elements.length == 4;
                normalCoords.add(Float.parseFloat(elements[1]));
                normalCoords.add(Float.parseFloat(elements[2]));
                normalCoords.add(Float.parseFloat(elements[3]));
            }
        }
        return normalCoords.size() == 0 ? null : ArrayUtils.toPrimitive(normalCoords.toArray(new Float[0]));
    }

    private static float[] transformVerticesWithDrawOrder(final float[] vertices, final short[] drawOrder) {
        ArrayList<Float> transformedVertices = new ArrayList<>();
        for(short i : drawOrder) {
            transformedVertices.add(vertices[3*i]);
            transformedVertices.add(vertices[3*i+1]);
            transformedVertices.add(vertices[3*i+2]);
        }
        return transformedVertices.size() == 0 ? null : ArrayUtils.toPrimitive(transformedVertices.toArray(new Float[0]));
    }

    public float[] getTexture() {
        return texture;
    }
}
