package com.slickcode.midnightdanger.objectproviders;

public abstract class AbstractObjectProvider {
    protected float[] vertices;
    protected float[] normals;

    public float[] getVertices() {
        return vertices;
    }
    public float[] getNormals() {
        return normals;
    }
}
