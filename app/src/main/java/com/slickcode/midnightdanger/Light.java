package com.slickcode.midnightdanger;

import android.opengl.Matrix;

import com.slickcode.midnightdanger.animators.Animatable;
import com.slickcode.midnightdanger.utils.ModelParameters;
import com.slickcode.midnightdanger.utils.Vector;

public class Light implements Animatable {
    private ModelParameters modelParameters;
    private Vector position;
    private Vector baseDirection;
    private Vector direction;
    private float cutOff = 0.0f;
    private Vector color;
    private float ambient;
    private float diffuse;
    private float specular;

    public Light(Vector position, Vector direction, float cutOff, Vector color, float ambient, float diffuse, float specular) {
        this.modelParameters = null;
        this.position = position;
        this.baseDirection = direction;
        this.direction = direction;
        this.cutOff = cutOff;
        this.color = color;
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
    }

    public Light(ModelParameters modelParameters, Vector position, Vector direction, float cutOff, Vector color, float ambient, float diffuse, float specular) {
        this.modelParameters = modelParameters;
        this.position = position;
        this.baseDirection = direction;
        this.direction = direction;
        this.cutOff = cutOff;
        this.color = color;
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
    }

    public Vector getPosition() {
        if(modelParameters != null) {
            return position.toWorldCoords(modelParameters);
        }
        return position;
    }
    public Vector getDirection() {
        if(modelParameters != null) {
            float[] rotationMatrix = modelParameters.createRotationMatrix();
            float[] dir = new float[4];
            Matrix.multiplyMV(dir, 0, rotationMatrix, 0, direction.toFloat4Array(), 0);
            return new Vector(dir);
        }
        return direction;
    }
    public float getCutOff() {
        return cutOff;
    }
    public Vector getColor() {
        return color;
    }
    public float getAmbient() {
        return ambient;
    }
    public float getDiffuse() {
        return diffuse;
    }
    public float getSpecular() {
        return specular;
    }

    @Override
    public void setPosition(Vector position) {
        this.position = position;
    }
    @Override
    public void setRotation(ModelParameters.Rotation rotation) {
        float[] rotationMatrix = new float[16];
        Matrix.setRotateM(rotationMatrix, 0, rotation.getAngle(), rotation.getX(), rotation.getY(), rotation.getZ());
        float[] rotatedVector = new float[4];
        Matrix.multiplyMV(rotatedVector, 0, rotationMatrix, 0, new float[] {baseDirection.getX(), baseDirection.getY(), baseDirection.getZ(), 1.0f}, 0);
        this.direction = new Vector(rotatedVector);
    }
}
