package com.slickcode.midnightdanger;

import android.content.Context;

public class GLSurfaceView extends android.opengl.GLSurfaceView {
    public GLSurfaceView(Context context) {
        super(context);
        setEGLContextClientVersion(2);
        Renderer renderer = new GLRenderer(context);
        setRenderer(renderer);
    }
}
