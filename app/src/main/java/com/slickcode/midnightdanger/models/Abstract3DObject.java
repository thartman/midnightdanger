package com.slickcode.midnightdanger.models;

import android.content.res.AssetManager;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.slickcode.midnightdanger.objectproviders.AbstractObjectProvider;
import com.slickcode.midnightdanger.animators.Animatable;
import com.slickcode.midnightdanger.Light;
import com.slickcode.midnightdanger.utils.ModelParameters;
import com.slickcode.midnightdanger.objectproviders.ObjObjectProvider;
import com.slickcode.midnightdanger.utils.Vector;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;
import java.util.Scanner;

public abstract class Abstract3DObject implements Animatable {
    protected static final String TAG_OPENGL = "OpenGL";
    private static final int SHADER_VERTEX_POSITION_LOCATION = 0;
    private static final int SHADER_NORMAL_COORDINATE_LOCATION = 1;
    protected int mProgram = -1;
    private int vertexShader;
    private int fragmentShader;
    private AbstractObjectProvider objectProvider;
    private FloatBuffer vertexBuffer;
    private FloatBuffer normalBuffer;
    private ModelParameters modelParameters;

    public Abstract3DObject(AssetManager assetManager,
                            String vertexShaderFilename,
                            String fragmentShaderFilename,
                            AbstractObjectProvider objectProvider,
                            ModelParameters modelParameters) throws IOException {
        this.objectProvider = objectProvider;
        this.modelParameters = modelParameters;
        vertexShader = Abstract3DObject.loadShader(GLES20.GL_VERTEX_SHADER, assetManager.open(vertexShaderFilename));
        fragmentShader = Abstract3DObject.loadShader(GLES20.GL_FRAGMENT_SHADER, assetManager.open(fragmentShaderFilename));
        createVertexBuffer(objectProvider.getVertices());
        createNormalBuffer(getObjectProvider().getNormals());
    }

    private void createVertexBuffer(float[] vertices) {
        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinate values * 4 bytes per float)
                vertices.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());
        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        // add the coordinates to the FloatBuffer
        vertexBuffer.put(vertices);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0);
    }
    private void createNormalBuffer(float[] normal) {
        ByteBuffer byteBuf = ByteBuffer.allocateDirect(normal.length * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        normalBuffer = byteBuf.asFloatBuffer();
        normalBuffer.put(normal);
        normalBuffer.position(0);
    }
    private static int loadShader(int type, InputStream code) {
        int shader = GLES20.glCreateShader(type);
        Log.v(TAG_OPENGL, "glCreateShader shader=" + shader);
        GLES20.glShaderSource(shader, Abstract3DObject.convertStreamToString(code));
        Log.v(TAG_OPENGL, "glShaderSource");
        GLES20.glCompileShader(shader);
        Log.v(TAG_OPENGL, "glCompileShader");
        return shader;
    }
    private static String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    protected AbstractObjectProvider getObjectProvider() {
        return objectProvider;
    }
    protected void setupOpenGl() {
        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();
        Log.v(TAG_OPENGL, "glCreateProgram");

        // add the vertex shader to mProgram
        GLES20.glAttachShader(mProgram, vertexShader);
        Log.v(TAG_OPENGL, "glAttachShader vertexShader");

        // add the fragment shader to mProgram
        GLES20.glAttachShader(mProgram, fragmentShader);
        Log.v(TAG_OPENGL, "glAttachShader fragmentShader");

        // bind vertex position
        GLES20.glBindAttribLocation(mProgram, SHADER_VERTEX_POSITION_LOCATION, "vertexPosition");
        Log.v(TAG_OPENGL, "glBindAttribLocation vertexPosition");
    }
    protected void setupOpenGlLinkProgramAndCleanup() {
        // enable depth buffer
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        // Use culling to remove back faces.
        GLES20.glEnable(GLES20.GL_CULL_FACE);

        // creates OpenGL ES mProgram executables
        GLES20.glLinkProgram(mProgram);
        Log.v(TAG_OPENGL, "glLinkProgram");

        // delete shaders after mProgram link
        GLES20.glDeleteShader(vertexShader);
        Log.v(TAG_OPENGL, "glDeleteShader vertexShader");
        GLES20.glDeleteShader(fragmentShader);
        Log.v(TAG_OPENGL, "glDeleteShader fragmentShader");
    }
    public void draw(float[] viewMatrix, float[] projectionMatrix, float[] viewPosition, List<Light> lights) {
        float[] modelMatrix = modelParameters.createModelMatrix();

        float[] normalMatrix = new float[16];
        Matrix.invertM(normalMatrix, 0, modelMatrix,0);
        Matrix.transposeM(normalMatrix, 0, normalMatrix, 0);

        // Add mProgram to OpenGL ES environment
        GLES20.glUseProgram(mProgram);
        Log.v(TAG_OPENGL, "glUseProgram mProgram=" + mProgram);

        // set model matrix
        int mModelMatrix = GLES20.glGetUniformLocation(mProgram, "modelMatrix");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mModelMatrix);
        GLES20.glUniformMatrix4fv(mModelMatrix, 1, false, modelMatrix, 0);
        Log.v(TAG_OPENGL, "glUniformMatrix4fv modelMatrix");

        // set view matrix
        int mViewMatrix = GLES20.glGetUniformLocation(mProgram, "viewMatrix");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mViewMatrix);
        GLES20.glUniformMatrix4fv(mViewMatrix, 1, false, viewMatrix, 0);
        Log.v(TAG_OPENGL, "glUniformMatrix4fv viewMatrix");

        // set projection matrix
        int mProjectionMatrix = GLES20.glGetUniformLocation(mProgram, "projectionMatrix");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mProjectionMatrix);
        GLES20.glUniformMatrix4fv(mProjectionMatrix, 1, false, projectionMatrix, 0);
        Log.v(TAG_OPENGL, "glUniformMatrix4fv projectionMatrix");

        // set normal matrix
        int mNormalMatrix = GLES20.glGetUniformLocation(mProgram, "normalMatrix");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mNormalMatrix);
        GLES20.glUniformMatrix4fv(mNormalMatrix, 1, false, normalMatrix, 0);
        Log.v(TAG_OPENGL, "glUniformMatrix4fv normalMatrix");

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(SHADER_VERTEX_POSITION_LOCATION);
        Log.v(TAG_OPENGL, "glEnableVertexAttribArray");
        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(SHADER_VERTEX_POSITION_LOCATION,
                ObjObjectProvider.COORDS_PER_VERTEX, GLES20.GL_FLOAT,
                false,
                ObjObjectProvider.COORDS_PER_VERTEX * 4,
                vertexBuffer);
        Log.v(TAG_OPENGL, "glVertexAttribPointer");

        // Enable a handle to normals coordinates
        GLES20.glEnableVertexAttribArray(SHADER_NORMAL_COORDINATE_LOCATION);
        Log.v(TAG_OPENGL, "glEnableVertexAttribArray");
        // Prepare texture coordinate data
        GLES20.glVertexAttribPointer(SHADER_NORMAL_COORDINATE_LOCATION,
                ObjObjectProvider.COORDS_PER_NORMAL_VERTEX,
                GLES20.GL_FLOAT,
                false,
                ObjObjectProvider.COORDS_PER_NORMAL_VERTEX * 4,
                normalBuffer);

        // set view position
        int mViewPosition = GLES20.glGetUniformLocation(mProgram, "viewPosition");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mViewPosition);
        GLES20.glUniform3fv(mViewPosition, 1, viewPosition, 0);
        Log.v(TAG_OPENGL, "glUniform3fv lightColor");

        for(int i = 0; i < lights.size(); i++) {
            setLight(i, lights.get(i));
        }
    }
    protected void doDraw() {
        // Draw vertices
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, objectProvider.getVertices().length / ObjObjectProvider.COORDS_PER_VERTEX);
        Log.v(TAG_OPENGL, "glDrawArrays");

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(0);
        Log.v(TAG_OPENGL, "glDisableVertexAttribArray");
    }
    private void setLight(int id, Light light) {
        // set light position
        int mLightPosition = GLES20.glGetUniformLocation(mProgram, "lights[" + id + "].position");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mLightPosition);
        GLES20.glUniform3fv(mLightPosition, 1, light.getPosition().toFloatArray(), 0);
        Log.v(TAG_OPENGL, "glUniform3fv lightPosition");

        // set lighting color
        int mLightColor = GLES20.glGetUniformLocation(mProgram, "lights[" + id + "].color");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mLightColor);
        GLES20.glUniform3fv(mLightColor, 1, light.getColor().toFloatArray(), 0);
        Log.v(TAG_OPENGL, "glUniform3fv lightColor");

        // set ambient strength
        int mAmbientStrength = GLES20.glGetUniformLocation(mProgram, "lights[" + id + "].ambientStrength");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mAmbientStrength);
        GLES20.glUniform1f(mAmbientStrength, light.getAmbient());
        Log.v(TAG_OPENGL, "glUniform1f ambientStrength");

        // set diffuse strength
        int mDiffuseStrength = GLES20.glGetUniformLocation(mProgram, "lights[" + id + "].diffuseStrength");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mDiffuseStrength);
        GLES20.glUniform1f(mDiffuseStrength, light.getDiffuse());
        Log.v(TAG_OPENGL, "glUniform1f diffuseStrength");

        // set specular strength
        int mSpecularStrength = GLES20.glGetUniformLocation(mProgram, "lights[" + id + "].specularStrength");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mSpecularStrength);
        GLES20.glUniform1f(mSpecularStrength, light.getSpecular());
        Log.v(TAG_OPENGL, "glUniform1f specularStrength");

        if(light.getDirection() != null) {
            // set direction
            int mDirection = GLES20.glGetUniformLocation(mProgram, "lights[" + id + "].direction");
            Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mDirection);
            GLES20.glUniform3fv(mDirection, 1, light.getDirection().toFloatArray(), 0);
            Log.v(TAG_OPENGL, "glUniform1f direction");
        }

        // set cut off
        int mCutOff = GLES20.glGetUniformLocation(mProgram, "lights[" + id + "].cutOff");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mCutOff);
        GLES20.glUniform1f(mCutOff, light.getCutOff());
        Log.v(TAG_OPENGL, "glUniform1f cutOff");
    }

    @Override
    public void setPosition(Vector position) {
        modelParameters.setPosition(position);
    }
    @Override
    public void setRotation(ModelParameters.Rotation rotation) {
        modelParameters.setRotation(rotation);
    }

    public ModelParameters getModelParameters() {
        return modelParameters;
    }
}
