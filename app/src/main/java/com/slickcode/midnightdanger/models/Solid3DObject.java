package com.slickcode.midnightdanger.models;

import android.content.res.AssetManager;
import android.opengl.GLES20;
import android.util.Log;

import com.slickcode.midnightdanger.Light;
import com.slickcode.midnightdanger.utils.ModelParameters;
import com.slickcode.midnightdanger.objectproviders.SolidObjectProvider;

import java.io.IOException;
import java.util.List;

public class Solid3DObject extends Abstract3DObject {
    public Solid3DObject(SolidObjectProvider objProvider,
                         AssetManager assetManager,
                         String vertexShaderFilename,
                         String fragmentShaderFilename,
                         ModelParameters modelParameters) throws IOException {
        super(assetManager, vertexShaderFilename, fragmentShaderFilename, objProvider, modelParameters);
        setupOpenGl();
    }

    @Override
    protected void setupOpenGl() {
        super.setupOpenGl();

        setupOpenGlLinkProgramAndCleanup();
    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix, float[] viewPosition, List<Light> lights) {
        super.draw(viewMatrix, projectionMatrix, viewPosition, lights);

        // set color
        int mColor = GLES20.glGetUniformLocation(mProgram, "color");
        Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mColor);
        GLES20.glUniform3fv(mColor, 1, getObjectProvider().getColor(), 0);
        Log.v(TAG_OPENGL, "glUniformMatrix4fv");

        doDraw();
    }

    @Override
    protected SolidObjectProvider getObjectProvider() {
        return (SolidObjectProvider) super.getObjectProvider();
    }
}
