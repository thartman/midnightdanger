package com.slickcode.midnightdanger.models;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

import com.slickcode.midnightdanger.Light;
import com.slickcode.midnightdanger.utils.ModelParameters;
import com.slickcode.midnightdanger.objectproviders.ObjObjectProvider;
import com.slickcode.midnightdanger.utils.Vector;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;

public class Obj3DObject extends Abstract3DObject {
    private static final int SHADER_TEXTURE_COORDINATE_LOCATION = 2;
    private FloatBuffer textureBuffer;
    private int textures[] = new int[1];
    private Bitmap textureBitmap;
    private Vector modelColor;

    public Obj3DObject(ObjObjectProvider objObjectProvider,
                       AssetManager assetManager,
                       String textureFilename,
                       String vertexShaderFilename,
                       String fragmentShaderFilename,
                       ModelParameters modelParameters,
                       Vector modelColor) throws IOException {
        super(assetManager,
                objObjectProvider.getTexture() != null ? vertexShaderFilename + "obj" : vertexShaderFilename + "solid",
                objObjectProvider.getTexture() != null ? fragmentShaderFilename + "obj" : fragmentShaderFilename + "solid",
                objObjectProvider,
                modelParameters);
        this.modelColor = modelColor;
        if(getObjectProvider().getTexture() != null) {
            createTextureBuffer(getObjectProvider().getTexture());
            textureBitmap = BitmapFactory.decodeStream(assetManager.open(textureFilename));
        }
        setupOpenGl();
    }
    public Obj3DObject(ObjObjectProvider objObjectProvider,
                       AssetManager assetManager,
                       String textureFilename,
                       String vertexShaderFilename,
                       String fragmentShaderFilename,
                       ModelParameters modelParameters) throws IOException {
        this(objObjectProvider, assetManager, textureFilename, vertexShaderFilename, fragmentShaderFilename, modelParameters, null);
    }
    private void createTextureBuffer(float[] texture) {
        ByteBuffer byteBuf = ByteBuffer.allocateDirect(texture.length * 4);
        byteBuf.order(ByteOrder.nativeOrder());
        textureBuffer = byteBuf.asFloatBuffer();
        textureBuffer.put(texture);
        textureBuffer.position(0);
    }

    @Override
    protected void setupOpenGl() {
        super.setupOpenGl();

        if(getObjectProvider().getTexture() != null) {
            // initialize texture
            GLES20.glGenTextures(1, textures, 0);
            Log.v(TAG_OPENGL, "glGenTextures textures[0]=" + textures[0]);
            if (textures[0] == 0) {
                Log.e(TAG_OPENGL, "glGenTextures was unable to generate texture");
            }
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0]);
            Log.v(TAG_OPENGL, "glBindTexture");
            // set the texture wrapping/filtering options (on the currently bound texture object)
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, textureBitmap, 0);
            Log.v(TAG_OPENGL, "texImage2D");
            textureBitmap.recycle();
            // bind texture coordinates position
            GLES20.glBindAttribLocation(mProgram, SHADER_TEXTURE_COORDINATE_LOCATION, "a_textureCoordinate");
            Log.v(TAG_OPENGL, "glBindAttribLocation a_textureCoordinate");
        }

        setupOpenGlLinkProgramAndCleanup();
    }

    @Override
    public void draw(float[] viewMatrix, float[] projectionMatrix, float[] viewPosition, List<Light>lights) {
        super.draw(viewMatrix, projectionMatrix, viewPosition, lights);

        if(getObjectProvider().getTexture() != null) {
            // Enable textures
            GLES20.glEnable(GLES20.GL_TEXTURE_2D);
            // Set the active texture unit to texture unit 0.
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            // Bind the texture to this unit.
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textures[0]);
            // set texture in fragment shader
            int textureUniformHandle = GLES20.glGetUniformLocation(mProgram, "texture");
            Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + textureUniformHandle);
            // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
            GLES20.glUniform1i(textureUniformHandle, 0);
            Log.v(TAG_OPENGL, "glUniform1i texture");

            // Enable a handle to texture coordinates
            GLES20.glEnableVertexAttribArray(SHADER_TEXTURE_COORDINATE_LOCATION);
            Log.v(TAG_OPENGL, "glEnableVertexAttribArray");
            // Prepare texture coordinate data
            GLES20.glVertexAttribPointer(SHADER_TEXTURE_COORDINATE_LOCATION,
                    ObjObjectProvider.COORDS_PER_TEXTURE_VERTEX,
                    GLES20.GL_FLOAT,
                    false,
                    ObjObjectProvider.COORDS_PER_TEXTURE_VERTEX * 4,
                    textureBuffer);
        } else {
            // set color
            int mColor = GLES20.glGetUniformLocation(mProgram, "color");
            Log.v(TAG_OPENGL, "glGetUniformLocation returned=" + mColor);
            GLES20.glUniform3fv(mColor, 1, modelColor.toFloatArray(), 0);
            Log.v(TAG_OPENGL, "glUniformMatrix4fv");
        }

        doDraw();
    }

    @Override
    protected ObjObjectProvider getObjectProvider() {
        return (ObjObjectProvider) super.getObjectProvider();
    }
}
