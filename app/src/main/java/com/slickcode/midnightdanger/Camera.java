package com.slickcode.midnightdanger;

import android.opengl.Matrix;

import com.slickcode.midnightdanger.animators.Animatable;
import com.slickcode.midnightdanger.utils.ModelParameters;
import com.slickcode.midnightdanger.utils.Vector;

public class Camera implements Animatable {
    private ModelParameters modelParameters;
    private boolean absolutePosition;
    private Vector position;
    private Vector center;
    private Vector up;

    public Camera(ModelParameters modelParameters, boolean absolutePosition, Vector position, Vector center, Vector up) {
        this.modelParameters = modelParameters;
        this.absolutePosition = absolutePosition;
        this.position = position;
        this.center = center;
        this.up = up;
    }

    public Camera(Vector position, Vector center, Vector up) {
        this(null, true, position, center, up);
    }

    public Vector getPosition() {
        if(modelParameters != null && !absolutePosition) {
            return position.toWorldCoords(modelParameters);
        }
        return position;
    }

    public Vector getCenter() {
        if(modelParameters != null) {
            return center.toWorldCoords(modelParameters);
        }
        return center;
    }

    public float[] getLookAtMatrix() {
        return getMyLookAtMatrix();

        /*float[] viewMatrix = new float[16];
        Vector position = getPosition();
        Vector center = getCenter();
        Matrix.setLookAtM(viewMatrix,
                0,
                position.getX(),
                position.getY(),
                position.getZ(),
                center.getX(),
                center.getY(),
                center.getZ(),
                up.getX(),
                up.getY(),
                up.getZ());
        return viewMatrix;*/
    }
    public float[] getMyLookAtMatrix() {
        Vector position = getPosition();

        Vector zAxis = Vector.subtract(position, getCenter());
        zAxis = Vector.normalize(zAxis);

        Vector up = Vector.normalize(this.up);
        Vector xAxis = Vector.cross(up, zAxis);
        xAxis = Vector.normalize(xAxis);

        Vector yAxis = Vector.cross(zAxis, xAxis);

        float[] viewMatrix = new float[] {
                xAxis.getX(), yAxis.getX(), zAxis.getX(), position.getX(),
                xAxis.getY(), yAxis.getY(), zAxis.getY(), position.getY(),
                xAxis.getZ(), yAxis.getZ(), zAxis.getZ(), position.getZ(),
                0, 0, 0, 1
        };
        Matrix.invertM(viewMatrix, 0, viewMatrix, 0);
        float[] viewMatrixTransposed = new float[16];
        Matrix.transposeM(viewMatrixTransposed, 0, viewMatrix, 0);
        return viewMatrixTransposed;
    }

    @Override
    public void setPosition(Vector position) {
        this.position = position;
    }
    // TODO: split Animatable interface into sub-interfaces?
    @Override
    public void setRotation(ModelParameters.Rotation rotation) {}
}
