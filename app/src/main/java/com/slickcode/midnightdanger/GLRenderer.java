package com.slickcode.midnightdanger;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.preference.PreferenceManager;
import android.util.Log;

import com.slickcode.midnightdanger.animators.Animator;
import com.slickcode.midnightdanger.animators.BounceAnimator;
import com.slickcode.midnightdanger.animators.CircleAnimator;
import com.slickcode.midnightdanger.models.Abstract3DObject;
import com.slickcode.midnightdanger.models.Obj3DObject;
import com.slickcode.midnightdanger.models.Solid3DObject;
import com.slickcode.midnightdanger.objectproviders.ObjObjectProvider;
import com.slickcode.midnightdanger.objectproviders.PlaneObjectProvider;
import com.slickcode.midnightdanger.utils.ModelParameters;
import com.slickcode.midnightdanger.utils.Vector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GLRenderer implements GLSurfaceView.Renderer {
    private static final String TAG = "OpenGL";
    private final float[] projectionMatrix = new float[16];
    private Context context;
    private List<Abstract3DObject> models = new ArrayList<>();
    private Abstract3DObject ship;
    private List<Light> lights = new ArrayList<>();
    private List<Animator> animators = new ArrayList<>();
    private List<Camera> cameras = new ArrayList<>();
    private final int CAMERA_CHANGE_PERIOD = 9000;
    private String lightingModel;
    private String shadingModel;
    private int camera;

    public GLRenderer(Context context) {
        this.context = context;
    }

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        lightingModel = sharedPreferences.getString(context.getResources().getString(R.string.pref_key_lighting_model), context.getResources().getString(R.string.pref_default_value_lighting_model));
        shadingModel = sharedPreferences.getString(context.getResources().getString(R.string.pref_key_shading_model), context.getResources().getString(R.string.pref_default_value_shading_model));
        camera = Integer.parseInt(sharedPreferences.getString(context.getResources().getString(R.string.pref_key_camera), context.getResources().getString(R.string.pref_default_value_camera)));

        // Set background clear color
        GLES20.glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
        Log.v(TAG, "glClearColor");
        // Create objects
        try {
            AssetManager assetManager = context.getAssets();
            Abstract3DObject cube = new Obj3DObject(new ObjObjectProvider(assetManager.open("cube.obj")),
                    assetManager, "cube.png",
                    "shading_" + shadingModel + "/vertex_shader_" + lightingModel + "_",
                    "shading_" + shadingModel + "/fragment_shader_" + lightingModel + "_",
                    new ModelParameters(new Vector(2.0f, 1.0f, 1.0f),
                            new ModelParameters.Rotation(0.0f, 1.0f, 0.0f, 45.0f),
                            null));
            models.add(cube);
            Abstract3DObject boat = new Obj3DObject(new ObjObjectProvider(assetManager.open("boat.obj")),
                    assetManager,
                    "boat.jpg",
                    "shading_" + shadingModel + "/vertex_shader_" + lightingModel + "_",
                    "shading_" + shadingModel + "/fragment_shader_" + lightingModel + "_",
                    new ModelParameters(new Vector(0.0f, 0.0f, 10.0f),
                            new ModelParameters.Rotation(0.0f, 1.0f, 0.0f, 45.0f),
                            new Vector(0.05f)));
            models.add(boat);

            Abstract3DObject lighthouse = new Obj3DObject(new ObjObjectProvider(assetManager.open("lighthouse.obj")),
                    assetManager,
                    "lighthouse.jpg",
                    "shading_" + shadingModel + "/vertex_shader_" + lightingModel + "_",
                    "shading_" + shadingModel + "/fragment_shader_" + lightingModel + "_",
                    new ModelParameters(new Vector(5.0f, 0.0f, 0.0f),
                    null,
                    new Vector(3.0f)));
            models.add(lighthouse);

            ship = new Obj3DObject(new ObjObjectProvider(assetManager.open("ship.obj")),
                    assetManager,
                    "ship.png",
                    "shading_" + shadingModel + "/vertex_shader_" + lightingModel + "_",
                    "shading_" + shadingModel + "/fragment_shader_" + lightingModel + "_",
                    new ModelParameters(new Vector(-5.0f, 0.0f, 0.0f),
                    new ModelParameters.Rotation(0.0f, 1.0f, 0.0f, -90.0f),
                    new Vector(2.0f)));
            models.add(ship);

            Abstract3DObject sea = new Solid3DObject(new PlaneObjectProvider(50.0f, 100.0f, new float[]{0.0f, 0.0f, 1.0f}),
                    assetManager,
                    "shading_" + shadingModel + "/vertex_shader_" + lightingModel + "_solid",
                    "shading_" + shadingModel + "/fragment_shader_" + lightingModel + "_solid",
                    new ModelParameters(new Vector(-25.0f, 0.0f, 0.0f), null, null));
            models.add(sea);

            Abstract3DObject sand = new Solid3DObject(new PlaneObjectProvider(50.0f, 100.0f, new float[]{1.0f, 1.0f, 0.0f}),
                    assetManager,
                    "shading_" + shadingModel + "/vertex_shader_" + lightingModel + "_solid",
                    "shading_" + shadingModel + "/fragment_shader_" + lightingModel + "_solid",
                    new ModelParameters(new Vector(25.0f, 0.0f, 0.0f), null, null));
            models.add(sand);

            Abstract3DObject torus = new Obj3DObject(new ObjObjectProvider(assetManager.open("torus.obj")),
                    assetManager,
                    null,
                    "shading_" + shadingModel + "/vertex_shader_" + lightingModel + "_",
                    "shading_" + shadingModel + "/fragment_shader_" + lightingModel + "_",
                    new ModelParameters(new Vector(5.0f, 0.25f, 7.0f),
                    null,
                    new Vector(1.75f)),
                    new Vector(0.2f, 0.2f, 0.2f));
            models.add(torus);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // set lights
        Light lighthouseLight = new Light(new Vector(5.0f, 3.0f, 0.0f),
                new Vector(-0.75f, -0.25f, 0.75f),
                (float) Math.cos(Math.toRadians(12.5)),
                new Vector(1.0f, 1.0f, 1.0f),
                0.0f,
                0.5f,
                0.5f);
        lights.add(lighthouseLight);
        Light shipLight = new Light(ship.getModelParameters(),
                new Vector(0.0f, 0.5f, -0.9f),
                new Vector(0.0f, -0.15f, -0.8f),
                (float) Math.cos(Math.toRadians(12.5)),
                new Vector(1.0f, 1.0f, 1.0f),
                0.0f,
                0.5f,
                0.5f);
        lights.add(shipLight);
        Light sceneLight = new Light(new Vector(0.0f, 3.0f, 30.0f),
                null,
                0.0f,
                new Vector(1.0f, 1.0f, 1.0f),
                0.05f,
                0.4f,
                0.7f);
        lights.add(sceneLight);

        // Set cameras
        cameras.add(new Camera(new Vector(5.0f, 3.0f, 15.0f),
                new Vector(0.0f, 0.0f, 0.0f),
                new Vector(0.0f, 1.0f, 0.0f)));
        cameras.add(new Camera(ship.getModelParameters(),
                false,
                new Vector(-0.75f, 2.0f, 4.0f),
                new Vector(2.5f, 0.0f, -5.0f),
                new Vector(0.0f, 1.0f, 0.0f)));
        cameras.add(new Camera(ship.getModelParameters(),
                true,
                new Vector(-10.0f, 9.0f, 30.0f),
                new Vector(0.0f, 0.0f, 0.0f),
                new Vector(0.0f, 1.0f, 0.0f)));

        // Set animations
        animators.add(new CircleAnimator(ship, 16000, new Vector(-10.0f, 0.0f, 0.0f), 5.0f));
        animators.add(new CircleAnimator(lighthouseLight, 4000, lighthouseLight.getPosition(), 0.0f));
        animators.add(new BounceAnimator(shipLight, 2000, 20.0f));
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        // Set viewport dimensions
        GLES20.glViewport(0, 0, width, height);
        Log.v(TAG, "glViewport");

        float ratio = (float) width / height;
        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(projectionMatrix, 0, -ratio, ratio, -1, 1, 3, 100);
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        // Clear viewport
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        Log.v(TAG, "glClear");

        // Update animations
        for(Animator animator : animators) {
            animator.onDrawFrame();
        }

        // Set view matrix
        Camera camera;
        if(this.camera == -1) {
            camera = cameras.get((int) (System.currentTimeMillis() % CAMERA_CHANGE_PERIOD / (CAMERA_CHANGE_PERIOD / cameras.size())));
        } else {
            camera = cameras.get(this.camera);
        }
        float[] viewMatrix = camera.getLookAtMatrix();

        // Draw objects
        for(Abstract3DObject model : models) {
            model.draw(viewMatrix, projectionMatrix, camera.getPosition().toFloatArray(), lights);
        }
    }
}
